#ifndef READ_VARS_H_
#define READ_VARS_H_

#include "simstructs.h"

void read_variables(Bar &bar, Model &model, ForceField &ffield);

void allocBarOnDevice(const Bar &bar, Bar &dev_bar);
void copyBarToDevice(const Bar &bar, Bar &dev_bar);
void freeBarOnDevice(Bar &dev_bar);

void allocForceFieldOnDevice(const ForceField ff, ForceField &dev_ff);
void copyForceFieldToDevice(const ForceField ff, ForceField &dev_ff);
void freeForceFieldOnDevice(ForceField &dev_ff);

void allocModelOnDevice(const Model &m, Model &dev_m);
void copyModelToDevice(const Model &m, Model &dev_m);
void freeModelOnDevice(Model &dev_m);

#endif //READ_VARS_H_
