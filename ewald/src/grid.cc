#include "grid.h"

#include <stdio.h>
#include <math.h>

#include <algorithm>

#include "point.h"

void Grid::orderToBars(Point * atom, int * id, const int n, const Point & box,
		double r_cut_off) {

	// number of cells on each edge
	n_bars_x = floor(box.x / r_cut_off);
	n_bars_y = floor(box.y / r_cut_off);
	n_bars_z = floor(box.z / r_cut_off);

	if (n_bars_x == 0)
		n_bars_x = 1;
	if (n_bars_y == 0)
		n_bars_y = 1;
	if (n_bars_z == 0)
		n_bars_z = 1;

	double bar_width_x = (box.x) / n_bars_x;
	double bar_width_y = (box.y) / n_bars_y;
	double bar_width_z = (box.z) / n_bars_y;

//   fprintf(stderr," %s:OK AT LINE %d \n",__FILE__,__LINE__);

	std::vector<int> x_idx(n_bars_x + 1, 0);
	std::vector<int> y_idx(n_bars_x * n_bars_y + 1, 0);
	std::vector<int> z_idx(n_bars_x * n_bars_y * n_bars_z + 1, 0);

	{
		int left = 0;
		int right = n;

		double left_border = -box.x / 2.0;

		for (int cell = 1; cell < n_bars_x; cell++) {
			x_idx[cell] = x_idx[cell - 1];
			for (int i = x_idx[cell]; i < right; i++) {

				if (atom[i].x < left_border + cell * bar_width_x) {
					Point tmp = atom[i];
					atom[i] = atom[x_idx[cell]];
					atom[x_idx[cell]] = tmp;

					int tmpid = id[i];
					id[i] = id[x_idx[cell]];
					id[x_idx[cell]] = tmpid;

					x_idx[cell]++;
				}

			}
		}
		x_idx[n_bars_x] = right;
	}

//   printf("----------------------after x sort----------------------------- xw = %f\n", bar_width_x);
//   for (int i =0;i<x_idx.size()-1;i++)
//   {
//     printf("Cell %d:\n", i);
//     for (int j=x_idx[i]; j < x_idx[i+1];j++)
//     {
//       printf("(%lf %lf %lf), %d\n", atom[j].x, atom[j].y, atom[j].z, id[j]);
//     }
//   }

	for (int k = 0; k < x_idx.size() - 1; k++) {
		int left = x_idx[k];
		int right = x_idx[k + 1];

		double left_border = -box.y / 2.0;
		for (int ycell = 1; ycell < n_bars_y; ycell++) {
			int cell = ycell + n_bars_y * k;
			y_idx[cell] = y_idx[cell - 1];
			for (int i = y_idx[cell - 1]; i < right; i++) {

				if (atom[i].y < left_border + ycell * bar_width_y) {
					Point tmp = atom[i];
					atom[i] = atom[y_idx[cell]];
					atom[y_idx[cell]] = tmp;

					int tmpid = id[i];
					id[i] = id[y_idx[cell]];
					id[y_idx[cell]] = tmpid;

					y_idx[cell]++;
				}
			}
		}
		y_idx[n_bars_y * k + n_bars_y] = right;
	}
	/* 
	 printf("----------------------after y sort----------------------------- yw = %f\n", bar_width_y);
	 for (int i =0;i<y_idx.size()-1;i++)
	 {
	 printf("Cell %d:\n", i);
	 for (int j=y_idx[i]; j < y_idx[i+1];j++)
	 {
	 printf("(%5.1lf %5.1lf %5.1lf), %d\n", atom[j].x, atom[j].y, atom[j].z, id[j]);
	 }
	 }
	 printf("--------------------------------------------------------------- \n");*/
	for (int k = 0; k < y_idx.size() - 1; k++) {
		int left = y_idx[k];
		int right = y_idx[k + 1];

		double left_border = -box.z / 2.0;
		for (int zcell = 1; zcell < n_bars_z; zcell++) {
			int cell = zcell + n_bars_y * k;
			z_idx.at(cell) = z_idx.at(cell - 1);
			for (int i = z_idx[cell - 1]; i < right; i++) {

				if (atom[i].z < left_border + zcell * bar_width_z) {
					Point tmp = atom[i];
					atom[i] = atom[z_idx[cell]];
					atom[z_idx[cell]] = tmp;

					int tmpid = id[i];
					id[i] = id[z_idx[cell]];
					id[z_idx[cell]] = tmpid;

					z_idx[cell]++;
				}
			}
		}
		z_idx[n_bars_y * k + n_bars_z] = right;
	}

	offset = z_idx;

}

int Grid::getSize(int i, int j, int k) const {
	int chunk_ind = k + j * n_bars_z + i * n_bars_z * n_bars_y;
	return offset[chunk_ind + 1] - offset[chunk_ind];
}

int Grid::getOffset(int i, int j, int k) const {
	return offset[k + j * n_bars_z + i * n_bars_z * n_bars_y];
}

int Grid::getChunksNumberX() const {
	return n_bars_x;
}

int Grid::getChunksNumberY() const {
	return n_bars_y;
}

int Grid::getChunksNumberZ() const {
	return n_bars_z;
}