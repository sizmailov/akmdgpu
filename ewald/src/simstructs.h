#ifndef SIMSTRUCTS_H
#define SIMSTRUCTS_H

#include "point.h"
#include "array2.h"

struct Bar {
	Point *p;  // pointer to first element of atoms in bar // atom 
	Point *f; // forces to be calculated                  // fmezh
	int n;     // number of elements                       // natoms

	int *id; // atom id i.e. it's serial number before reorders 

	double potees, poteLJ, poteHb, vireLJ, vireHb;
	Array2Dbl imolen;
};

struct Model {
	int natoms;
	int matoms, ntypes, mtypes, nmolec, mmolec, st2bns;
	int *indexe, *moleat, *bigmol, *elmode, *nextat;
	int *intype;
	Point box, boxi;
	double r2cuto;
	double alpha;
	double *one;

	int kfourier;
	double kfdelta;
};

struct MathSpeedup {
	int erfcar, nerfc;
	double b1, a1, a2, a3, a4, a5, totpi, faerfc;
	double *arerfc, *prerfc;
};

struct ForceField {
	int mmodes;
	Array2Int hbonded;
	Array2Dbl sixi, twli, ljdeli, six, twl, ljdel, hba, hbb;
};

struct MPIConstants {
	int MPIMYID, MPINPRC;
};

#endif
