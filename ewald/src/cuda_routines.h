#ifndef CUDA_ROUTINES_H
#define CUDA_ROUTINES_H

#include "simstructs.h"
#include "readvars.h"

// #include "gpuassert.h"

struct GPU_Info {
	int count;
	Bar *bar;
	Model *model;
	ForceField *forcefield;

	double **pes, **pLJ, **pHb, **vHb, **vLJ;
};

struct int_coord_3 {
	int x, y, z;
};

extern GPU_Info GPU_Info;

class Grid;

void init_cuda(const Bar &bar, const Model & model, const ForceField & ff);
void calculate_force_cuda(const Grid & grid, Bar & bar, int_coord_3 begin,
		int_coord_3 end);

void calcFoureir(Bar bar, Model model);
void calcFourierForce_CPU(Bar bar,Model model);

void finalize_cuda();

#endif //CUDA_ROUTINES_H
