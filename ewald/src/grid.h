#ifndef MD_GRID3D_H_
#define MD_GRID3D_H_
#include <vector>
#include "simstructs.h"
class Point;
// class Bar;

class Grid {
	//
	// provides split atoms to cube volumes with r_cut_off width
	//

public:
	void orderToBars(Point * atom, int * id, const int n, const Point & box,
			double r_cut_off);
	int getSize(int i, int j, int k) const;
	int getOffset(int i, int j, int k) const;
	int getChunksNumberX() const;
	int getChunksNumberY() const;
	int getChunksNumberZ() const;

private:

	int n_bars_x;                 // number of bars over x-axis
	int n_bars_y;                 // number of bars over y-axis
	int n_bars_z;                 // number of bars over z-axis

	std::vector<int> offset; // offset[ k  +  j * n_bars_z  +  i * n_bars_z * n_bars_y ]    indicates first atom in atom array in (i,j,k) chunk

};

#endif
