// 
// Includes wrapper-classes for fortran 2d-arrays
// 

#ifndef ARRAY2_H_
#define ARRAY2_H_

#include "array2dbl.h"
#include "array2int.h"

#define arr_val(array,i,j) ( array.p[i+j*array.n])

#endif //ARRAY2_H_
