#include "cuda_routines.h"
#include <cstring>

#include <cuda.h> 
#include "grid.h"
#include "gpuassert.h"

const int maxThreadsPerBlock_force_calc = 64;
const int maxThreadsPerBlock_force_sum = 512;
const int maxThreadsPerBlock_fourierSinCos = 64;
const int maxThreadsPerBlock_fourier_force = 64;
const double PI = 3.141592653589793238462643383;

__device__ double idint(double a);
__device__ double dsign(double a, double b);
__global__ void calcForce(Bar bar, Bar bar2, Model model, ForceField ff,
		double *pes, double * pLJ, double *pHb, double * vHb, double *vLJ);
__global__ void sum(double * a, int n);

__global__ void calcFourierCellSinCos(Bar bar, Model dev_model, double *_scos, double *_ssin, int3 kf);
__global__ void calcFourierForce(Bar bar, Model model, double *_scos, double *_ssin, int3 kf);


void init_cuda(const Bar &bar, const Model & model, const ForceField & ff) {

	cudaGetDeviceCount(&GPU_Info.count);

	GPU_Info.bar = (Bar *) malloc(GPU_Info.count * sizeof(ForceField));
	GPU_Info.model = (Model *) malloc(GPU_Info.count * sizeof(Model));
	GPU_Info.forcefield = (ForceField *) malloc(
			GPU_Info.count * sizeof(ForceField));

	GPU_Info.pes = (double **) malloc(GPU_Info.count * sizeof(double *));
	GPU_Info.pLJ = (double **) malloc(GPU_Info.count * sizeof(double *));
	GPU_Info.pHb = (double **) malloc(GPU_Info.count * sizeof(double *));
	GPU_Info.vHb = (double **) malloc(GPU_Info.count * sizeof(double *));
	GPU_Info.vLJ = (double **) malloc(GPU_Info.count * sizeof(double *));

	for (int i = 0; i < GPU_Info.count; i++) {
		cudaSetDevice(i);

		allocBarOnDevice(bar, GPU_Info.bar[i]);
		allocModelOnDevice(model, GPU_Info.model[i]);
		allocForceFieldOnDevice(ff, GPU_Info.forcefield[i]);

		copyBarToDevice(bar, GPU_Info.bar[i]);
		copyModelToDevice(model, GPU_Info.model[i]);
		copyForceFieldToDevice(ff, GPU_Info.forcefield[i]);

		gpuErrchk(cudaMalloc((void** )&GPU_Info.pes[i], sizeof(double) * bar.n));
		gpuErrchk(cudaMalloc((void** )&GPU_Info.pLJ[i], sizeof(double) * bar.n));
		gpuErrchk(cudaMalloc((void** )&GPU_Info.pHb[i], sizeof(double) * bar.n));
		gpuErrchk(cudaMalloc((void** )&GPU_Info.vHb[i], sizeof(double) * bar.n));
		gpuErrchk(cudaMalloc((void** )&GPU_Info.vLJ[i], sizeof(double) * bar.n));
	}
}

void calculate_force_cuda(const Grid & grid, Bar & bar, int_coord_3 begin,
		int_coord_3 end) {
//   cudaSetDevice(0);
//   cudaEvent_t start,stop;
//   gpuErrchk( cudaEventCreate(&start) );
//   gpuErrchk( cudaEventCreate(&stop) );
//   
//   gpuErrchk( cudaEventRecord(start,0) );

	const int streams_per_device = 1;
	cudaStream_t stream[GPU_Info.count][streams_per_device];

	for (int i = 0; i < GPU_Info.count; i++) {
		cudaSetDevice(i);
		copyBarToDevice(bar, GPU_Info.bar[i]);
		gpuErrchk(cudaMemset(GPU_Info.bar[i].f, 0, sizeof(Point) * bar.n));

		gpuErrchk(
				cudaMemset(GPU_Info.pes[i], 0,
						sizeof(GPU_Info.pes[i][0]) * bar.n));
		gpuErrchk(
				cudaMemset(GPU_Info.pLJ[i], 0,
						sizeof(GPU_Info.pLJ[i][0]) * bar.n));
		gpuErrchk(
				cudaMemset(GPU_Info.pHb[i], 0,
						sizeof(GPU_Info.pHb[i][0]) * bar.n));
		gpuErrchk(
				cudaMemset(GPU_Info.vHb[i], 0,
						sizeof(GPU_Info.vHb[i][0]) * bar.n));
		gpuErrchk(
				cudaMemset(GPU_Info.vLJ[i], 0,
						sizeof(GPU_Info.vLJ[i][0]) * bar.n));

		for (int j = 0; j < streams_per_device; j++) {
			gpuErrchk(cudaStreamCreate(&stream[i][j]));
		}
	}

	int blocksPerX = grid.getChunksNumberX();
	int blocksPerY = grid.getChunksNumberY();
	int blocksPerZ = grid.getChunksNumberZ();

	int left_x_neighboor = -1, right_x_neighboor = 2;
	int left_y_neighboor = -1, right_y_neighboor = 2;
	int left_z_neighboor = -1, right_z_neighboor = 2;

	if (blocksPerX == 1) {
		left_x_neighboor = 0;
		right_x_neighboor = 1;
	}
	if (blocksPerX == 2) {
		left_x_neighboor = -1;
		right_x_neighboor = 1;
	}

	if (blocksPerY == 1) {
		left_y_neighboor = 0;
		right_y_neighboor = 1;
	}
	if (blocksPerY == 2) {
		left_y_neighboor = -1;
		right_y_neighboor = 1;
	}

	if (blocksPerZ == 1) {
		left_z_neighboor = 0;
		right_z_neighboor = 1;
	}
	if (blocksPerZ == 2) {
		left_z_neighboor = -1;
		right_z_neighboor = 1;
	}

	Bar bar1[GPU_Info.count];
	Bar bar2[GPU_Info.count];

	int currentDevice = 0;
	int currentStream[GPU_Info.count];
	for (int i = 0; i < GPU_Info.count; i++)
		currentStream[i] = 0;

	for (int i = begin.x; i < end.x; i++) {
		for (int j = begin.y; j < end.y; j++) {
			for (int k = begin.z; k < end.z; k++) {
				cudaSetDevice(currentDevice);

				int offset1 = grid.getOffset(i, j, k);

				bar1[currentDevice].p = GPU_Info.bar[currentDevice].p + offset1;
				bar1[currentDevice].f = GPU_Info.bar[currentDevice].f + offset1;
				bar1[currentDevice].id = GPU_Info.bar[currentDevice].id
						+ offset1;

				bar1[currentDevice].n = grid.getSize(i, j, k);

				for (int di = left_x_neighboor; di < right_x_neighboor; di++) {
					for (int dj = left_y_neighboor; dj < right_y_neighboor;
							dj++) {
						for (int dk = left_z_neighboor; dk < right_z_neighboor;
								dk++) {
							int i2 = (i + di + blocksPerX) % blocksPerX;
							int j2 = (j + dj + blocksPerY) % blocksPerY;
							int k2 = (k + dk + blocksPerZ) % blocksPerZ;

							int offset2 = grid.getOffset(i2, j2, k2);

							bar2[currentDevice].p =
									GPU_Info.bar[currentDevice].p + offset2;
							bar2[currentDevice].f =
									GPU_Info.bar[currentDevice].f + offset2;
							bar2[currentDevice].id =
									GPU_Info.bar[currentDevice].id + offset2;

							bar2[currentDevice].n = grid.getSize(i2, j2, k2);

							printf(
									"f (%d,%d,%d) <-> (%d,%d,%d) : (%d <-> %d) on Device %d\n",
									i, j, k, i2, j2, k2, bar1[currentDevice].n,
									bar2[currentDevice].n, currentDevice);
//               int threadNum = bar2[currentDevice].n;
							int threadNum = std::min(maxThreadsPerBlock_force_calc,
									bar2[currentDevice].n);
//               calc_count ++;
							calcForce<<<bar1[currentDevice].n, threadNum,
									currentStream[currentDevice]>>>(
									bar1[currentDevice], bar2[currentDevice],
									GPU_Info.model[currentDevice],
									GPU_Info.forcefield[currentDevice],
									GPU_Info.pes[currentDevice],
									GPU_Info.pLJ[currentDevice],
									GPU_Info.pHb[currentDevice],
									GPU_Info.vHb[currentDevice],
									GPU_Info.vLJ[currentDevice]);
						}
					}
				}
				currentStream[currentDevice] =
						(currentStream[currentDevice] + 1) % streams_per_device;
				currentDevice = (currentDevice + 1) % GPU_Info.count;
			}
		}
	}

//   printf("calc Force calls count = \%d n", calc_count);

	for (int i = 0; i < GPU_Info.count; i++) {
		gpuErrchk(cudaSetDevice(i));
		for (int j = 0; j < streams_per_device; j++)
			gpuErrchk(cudaStreamSynchronize(stream[i][j]));
	}

	int threadNum = std::min(bar.n, maxThreadsPerBlock_force_sum);

	for (currentDevice = 0; currentDevice < GPU_Info.count; currentDevice++) {
		cudaSetDevice(currentDevice);
		int st = 0;
		sum<<<1, threadNum, st>>>(GPU_Info.pes[currentDevice], bar.n);
		st = (st + 1) % streams_per_device;
		sum<<<1, threadNum, st>>>(GPU_Info.pLJ[currentDevice], bar.n);
		st = (st + 1) % streams_per_device;
		sum<<<1, threadNum, st>>>(GPU_Info.pHb[currentDevice], bar.n);
		st = (st + 1) % streams_per_device;
		sum<<<1, threadNum, st>>>(GPU_Info.vLJ[currentDevice], bar.n);
		st = (st + 1) % streams_per_device;
		sum<<<1, threadNum, st>>>(GPU_Info.vHb[currentDevice], bar.n);
		st = (st + 1) % streams_per_device;
	}

	bar.potees = 0;
	bar.poteLJ = 0;
	bar.poteHb = 0;
	bar.vireHb = 0;
	bar.vireLJ = 0;

	double pes, pLJ, pHb, vHb, vLJ;

	for (currentDevice = 0; currentDevice < GPU_Info.count; currentDevice++) {
		cudaSetDevice(currentDevice);
		gpuErrchk(
				cudaMemcpy(&pes, GPU_Info.pes[currentDevice], sizeof(double),
						cudaMemcpyDeviceToHost));
		gpuErrchk(
				cudaMemcpy(&pLJ, GPU_Info.pLJ[currentDevice], sizeof(double),
						cudaMemcpyDeviceToHost));
		gpuErrchk(
				cudaMemcpy(&pHb, GPU_Info.pHb[currentDevice], sizeof(double),
						cudaMemcpyDeviceToHost));
		gpuErrchk(
				cudaMemcpy(&vLJ, GPU_Info.vLJ[currentDevice], sizeof(double),
						cudaMemcpyDeviceToHost));
		gpuErrchk(
				cudaMemcpy(&vHb, GPU_Info.vHb[currentDevice], sizeof(double),
						cudaMemcpyDeviceToHost));

		bar.potees += pes;
		bar.poteLJ += pLJ;
		bar.poteHb += pHb;
		bar.vireHb += vHb;
		bar.vireLJ += vLJ;
	}
	bar.potees *= 0.5;
	bar.poteLJ *= 0.5;
	bar.poteHb *= 0.5;
	bar.vireHb *= 0.5;
	bar.vireLJ *= 0.5;

	currentDevice = 0;

	for (int i = begin.x; i < end.x; i++) {
		for (int j = begin.y; j < end.y; j++) {
			for (int k = begin.z; k < end.z; k++) {
				int offset = grid.getOffset(i, j, k);
				int n = grid.getSize(i, j, k);
				cudaSetDevice(currentDevice);
				gpuErrchk(
						cudaMemcpy(bar.f + offset,
								GPU_Info.bar[currentDevice].f + offset,
								n * sizeof(Point), cudaMemcpyDeviceToHost));
				currentDevice = (currentDevice + 1) % GPU_Info.count;
			}
		}
	}

//   cudaSetDevice(0);
//   gpuErrchk( cudaEventRecord(stop,0) );
//   gpuErrchk( cudaEventSynchronize(stop) );
// 
//   float elapsedTime;
// 
//   gpuErrchk( cudaEventElapsedTime(&elapsedTime,start,stop) );  
//   gpuErrchk( cudaEventDestroy(start) );
//   gpuErrchk( cudaEventDestroy(stop) );

//   fprintf(stderr,"%3.2f ms\n",elapsedTime);
}


void finalize_cuda() {
	for (int i = 0; i < GPU_Info.count; i++) {
		cudaSetDevice(i);
		freeBarOnDevice(GPU_Info.bar[i]);
		freeModelOnDevice(GPU_Info.model[i]);
		freeForceFieldOnDevice(GPU_Info.forcefield[i]);

		gpuErrchk(cudaFree(GPU_Info.pes[i]));
		gpuErrchk(cudaFree(GPU_Info.pLJ[i]));
		gpuErrchk(cudaFree(GPU_Info.pHb[i]));
		gpuErrchk(cudaFree(GPU_Info.vHb[i]));
		gpuErrchk(cudaFree(GPU_Info.vLJ[i]));
	}

	free(GPU_Info.bar);
	free(GPU_Info.model);
	free(GPU_Info.forcefield);

	free(GPU_Info.pes);
	free(GPU_Info.pLJ);
	free(GPU_Info.pHb);
	free(GPU_Info.vHb);
	free(GPU_Info.vLJ);

}

void calcFoureir(Bar bar, Model model)
{


	Bar dev_bar;
	allocBarOnDevice(bar,dev_bar);
	copyBarToDevice(bar,dev_bar);

	gpuErrchk( cudaMemset(dev_bar.f,0,sizeof(dev_bar.f[0]) * bar.n) );

	double x = 2.0 * PI / model.box.x;
	double y = 2.0 * PI / model.box.y;
	double z = 2.0 * PI / model.box.z;

	double kmax = std::min(std::min( x, y),z) * model.kfourier ;


	int3 kf;

	kf.x  = ceil( kmax/x*model.kfdelta+0.5 );
    kf.y  = ceil( kmax/y*model.kfdelta+0.5 );
    kf.z  = ceil( kmax/z*model.kfdelta+0.5 );

    printf("kf = ( %d, %d, %d)\n\n",kf.x,kf.y,kf.z);

	int ncells = (kf.x+1)*(kf.y*2+1)*(kf.z*2+1);

	double *_scos;// sum over all atoms of cos( ... )
	double *_ssin;

	gpuErrchk(cudaMalloc((void** )&_scos, sizeof(double) * ncells));
	gpuErrchk(cudaMalloc((void** )&_ssin, sizeof(double) * ncells));


	dim3 grid(2*kf.x+1,2*kf.y+1,2*kf.z+1);
	printf("fourier grid: (%5d %5d %5d)\n\n",grid.x,grid.y,grid.z);
	calcFourierCellSinCos<<<grid,maxThreadsPerBlock_fourierSinCos>>>(dev_bar,GPU_Info.model[0],_scos,_ssin,kf);

	calcFourierForce<<<bar.n, 1 >>>(dev_bar,GPU_Info.model[0],_scos,_ssin,kf);



	gpuErrchk(cudaFree(_scos));
	gpuErrchk(cudaFree(_ssin));

	gpuErrchk(cudaMemcpy(  &bar.f[0],&dev_bar.f[0],sizeof(Point)*bar.n,cudaMemcpyDeviceToHost));



}





struct GPU_Info GPU_Info;

__global__ void calcForce(Bar bar, Bar bar2, Model model, ForceField ff,
		double *pes, double * pLJ, double *pHb, double * vHb, double *vLJ) {

	int n = bar.n;
	int n2 = bar2.n;

	int i = blockIdx.x;
	int j = threadIdx.x;

	int iId = bar.id[i];
	int jId;

	__shared__ Point force[maxThreadsPerBlock_force_calc];
	__shared__ double vireLJ[maxThreadsPerBlock_force_calc];
	__shared__ double vireHb[maxThreadsPerBlock_force_calc];
	__shared__ double poteLJ[maxThreadsPerBlock_force_calc];
	__shared__ double potees[maxThreadsPerBlock_force_calc];
	__shared__ double poteHb[maxThreadsPerBlock_force_calc];

	int tid = threadIdx.x;

	if (i < n) {
		force[tid].x = 0.0;
		force[tid].y = 0.0;
		force[tid].z = 0.0;

		vireLJ[tid] = 0;
		vireHb[tid] = 0;
		poteLJ[tid] = 0;
		potees[tid] = 0;
		poteHb[tid] = 0;

		int ityp, jtyp;
		int ibig;
		int imode, jmode;
		double oni, onj;

		double pbx2, pby2, pbz2;
		double mbx2, mby2, mbz2;

		pbx2 = model.box.x * 0.5;
		mbx2 = -pbx2;
		pby2 = model.box.y * 0.5;
		mby2 = -pby2;
		pbz2 = model.box.z * 0.5;
		mbz2 = -pbz2;

		double r, r1, r2, r6, r10, r1true, r2del;
		double Qijr;
		const double totpi = 1.1283791670955125738961; //   = 2/sqrt(pi)
		double alpha, alphar;
		double erfc_alphar;

		double fe, fl, fh, f, ftmp;
		double Aij, Bij;
		double delta;

		int imol = model.moleat[iId];

		ityp = model.intype[model.moleat[iId] - 1] - 1;
		ibig = model.bigmol[ityp];
		imode = model.elmode[iId] - 1;
		oni = model.one[iId];

		while (j < n2) {
			jId = bar2.id[j];

			if ((iId != jId) && imol != model.moleat[jId]) {

				jtyp = model.intype[model.moleat[jId] - 1] - 1;

				jmode = model.elmode[jId] - 1;

				onj = model.one[jId];

				Point dr;

				if (!(ityp == model.st2bns - 1 && jtyp == model.st2bns - 1)) {

					// distance
					dr.x = bar.p[i].x - bar2.p[j].x;
					dr.y = bar.p[i].y - bar2.p[j].y;
					dr.z = bar.p[i].z - bar2.p[j].z;
					//printf("(%e,%e,%e)\n",dx,dy,dz); return;
					// charge
					// nearest image
					if (ibig || model.bigmol[jtyp]) {
						dr.x -= model.box.x
								* dsign(
										(double) idint(
												fabs(dr.x * model.boxi.x)
														+ 0.5), dr.x);
						dr.y -= model.box.y
								* dsign(
										(double) idint(
												fabs(dr.y * model.boxi.y)
														+ 0.5), dr.y);
						dr.z -= model.box.z
								* dsign(
										(double) idint(
												fabs(dr.z * model.boxi.z)
														+ 0.5), dr.z);
					} else {
						if (dr.x > pbx2)
							dr.x -= model.box.x;
						if (dr.x < mbx2)
							dr.x += model.box.x;
						if (dr.y > pby2)
							dr.y -= model.box.y;
						if (dr.y < mby2)
							dr.y += model.box.y;
						if (dr.z > pbz2)
							dr.z -= model.box.z;
						if (dr.z < mbz2)
							dr.z += model.box.z;
					}

					r2 = dr.x * dr.x + dr.y * dr.y + dr.z * dr.z;

					if (r2 <= model.r2cuto) {
//            printf("dr(%4d,%4d) = %f  %f  %f\n",i,j,dr.x,dr.y,dr.z);
						if (onj != 0.0 && oni != 0.0) //both atoms charged
								{
							r = sqrt(r2);
							r2 = 1.0 / r2; // from now r2 is INVERTED square of distance
							Qijr = oni * onj / r;
							alphar = model.alpha * r;
							erfc_alphar = erfc(alphar);
//              printf("erfc(%d , %d) = %e\n", i, j,erfc_alphar);
//            printf("Qijr(%d , %d) = %e\n", i, j,Qijr);
							potees[tid] += Qijr * erfc_alphar;
							fe = Qijr
									* (totpi * alphar * exp(-alphar * alphar)
											+ erfc_alphar) * r2;
						} else {
							r2 = 1.0 / r2;
							fe = 0.0;
						}

						if (model.moleat[iId] == model.moleat[jId]) {
							Aij = arr_val(ff.sixi,imode,jmode);
							Bij = arr_val(ff.twli,imode,jmode);
							delta = arr_val(ff.ljdeli,ityp, jtyp);
						}
						else
						{
							Aij = arr_val(ff.six,imode,jmode);
							Bij = arr_val(ff.twl,imode,jmode);
							delta = arr_val(ff.ljdel,ityp, jtyp);
						}

						r1true = sqrt(1.0 / r2);
						r2del = (r1true - delta) * (r1true - delta);
						r2del = 1.0 / r2del;
						r6 = r2del * r2del * r2del;

						ftmp = (Aij + Bij * r6) * r6;
						poteLJ[tid] += ftmp;

						fl = (6.0 * Aij + 12.0 * Bij * r6) * r6 * sqrt(r2del)
								/ r1true;
						vireLJ[tid] += fl / r2;

						fh = 0.0;
						if (arr_val(ff.hbonded,imode,jmode))
						{
							Aij = arr_val(ff.hba,imode,jmode);
							Bij = arr_val(ff.hbb,imode,jmode);
							r10 = pow(r2, 5.0);
							// H-bonded energy
							//            bar.imolen(ityp,jtyp) += ftmp;
							// forces
							ftmp = ( Aij*r2 + Bij) * r10;
							poteHb[tid] += ftmp;

							fh = (12.0 * Aij * r2 + 10.0 * Bij) * r10 * r2;
							vireHb[tid] += fh/r2;
						}
						f = fl + fe + fh;
						force[tid].x += f * dr.x;
						force[tid].y += f * dr.y;
						force[tid].z += f * dr.z;
					}
				}
			}
			j += blockDim.x;
		}

		__syncthreads();
		int sum_len = 1;
		while (sum_len < blockDim.x)
			sum_len *= 2;
		while (sum_len > 0) {
			sum_len /= 2;
			if (tid < sum_len && tid + sum_len < blockDim.x) {
				force[tid].x += force[tid + sum_len].x;
				force[tid].y += force[tid + sum_len].y;
				force[tid].z += force[tid + sum_len].z;

				vireLJ[tid] += vireLJ[tid + sum_len];
				vireHb[tid] += vireHb[tid + sum_len];
				poteLJ[tid] += poteLJ[tid + sum_len];
				potees[tid] += potees[tid + sum_len];
				poteHb[tid] += poteHb[tid + sum_len];
			}
			__syncthreads();
		}
		if (tid == 0) {
			bar.f[i].x += force[0].x;
			bar.f[i].y += force[0].y;
			bar.f[i].z += force[0].z;

			vLJ[i] += vireLJ[0];
			vHb[i] += vireHb[0];
			pLJ[i] += poteLJ[0];
			pes[i] += potees[0];
			pHb[i] += poteHb[0];
		}
	}
}

__device__ double dsign(double a, double b) {
	double s;
	s = (b > 0.0) ? 1.0 : -1.0;
	return fabs(a) * s;
}

__device__ double idint(double a) {
	return (a > 0) ? floor(a) : ceil(a);
}

__global__ void sum(double * a, int n) {

	int tid = threadIdx.x;
	int indx = tid;
	if (tid < n) {

		__shared__ double tmp[maxThreadsPerBlock_force_sum];

		tmp[tid] = 0.0;

		while (indx < n) {
			tmp[tid] += a[indx];
			indx += blockDim.x;
		}
		__syncthreads();
		int j = 1;
		while (j < blockDim.x)
			j *= 2;

		while (j > 0) {
			j = j / 2;
			if (tid < j && tid + j < blockDim.x) {
				tmp[tid] += tmp[tid + j];
			}
			__syncthreads();
		}
		if (tid == 0) {
			a[0] = tmp[0];
		}
	}
}

__global__ void calcFourierCellSinCos(Bar bar, Model dev_model, double *_scos, double *_ssin, int3 kf) {

	int l = blockIdx.x;
	int m = blockIdx.y-kf.y;
	int n = blockIdx.z-kf.z;

	if ((l!=0 || m!=0 || n!=0 ) && ( (l!=0 || m!=0)||n>0))
	{

		uint tid = threadIdx.x;

		double lx = 2.0 * PI / dev_model.box.x;
		double my = 2.0 * PI / dev_model.box.y;
		double nz = 2.0 * PI / dev_model.box.z;
		double ralph = -0.25 / (dev_model.alpha * dev_model.alpha);

		double minxyz = lx < my ? lx : my;
		minxyz = nz < minxyz ? nz : minxyz;

		lx *= l;
		my *= m;
		nz *= n;


		if (    lx*lx +
				my*my +
				nz*nz  <= dev_model.kfourier * dev_model.kfourier * minxyz * minxyz * dev_model.kfdelta)
		{
			__shared__ double scos[maxThreadsPerBlock_fourierSinCos];
			__shared__ double ssin[maxThreadsPerBlock_fourierSinCos];

			scos[tid] = 0;
			ssin[tid] = 0;

			int ind = threadIdx.x;
			while (ind<bar.n)
			{
				double t = lx*bar.p[ind].x  + my*bar.p[ind].y +  nz*bar.p[ind].z;
				scos[tid]+=cos(t)*dev_model.one[bar.id[ind]];
				ssin[tid]+=sin(t)*dev_model.one[bar.id[ind]];
				ind += blockDim.x;
			}
			__syncthreads();

			int half = 1;
//			if (tid==0 && l==0 && m==0) printf("half = %d     \tblockDim.x = %d\n",half,blockDim.x);
			while (half < blockDim.x)
			{
				half *= 2;
//				if (tid==0 && l==0 && m==0) printf("half = %d     \tblockDim.x = %d\n",half,blockDim.x);
			}

			while (half > 0) {
				half = half / 2;
				if (tid < half && tid + half < blockDim.x) {
//					if (tid==0 && l==0 && m==0) printf("half = %d     \tblockDim.x = %d\n",half,blockDim.x);
					scos[tid] += scos[tid + half];
					ssin[tid] += ssin[tid + half];
				}
				__syncthreads();
			}
			if (tid==0)
			{
				_ssin[blockIdx.x*gridDim.y*gridDim.z+blockIdx.y*gridDim.z + blockIdx.z] = ssin[0];
				_scos[blockIdx.x*gridDim.y*gridDim.z+blockIdx.y*gridDim.z + blockIdx.z] = scos[0];
			}

		}
	}
}


__global__ void calcFourierForce(Bar bar,const Model model, double *_scos, double *_ssin, int3 kf)
{

	uint ind = blockIdx.x;
	if (ind<bar.n){

		double x = 2.0 * PI / model.box.x;
		double y = 2.0 * PI / model.box.y;
		double z = 2.0 * PI / model.box.z;

		double ralph = -0.25 / (model.alpha * model.alpha);

		double minxyz = x < y ? x : y;
		minxyz = z < minxyz ? z : minxyz;

		__shared__ Point tmp[maxThreadsPerBlock_fourier_force];

		int nmin =1;
		int mmin =0;

//		if (ind ==0) printf("lmn: %d, %d, %d\n",kf.x,kf.y,kf.z);
		for (int l =0;l<=kf.x;l++){
			for (int m=mmin;m<=kf.y;m++){
				for (int n=nmin;n<=kf.z;n++){

					if (    l*x*l*x + m*y*m*y + n*z*n*z
							<= model.kfourier * model.kfourier * minxyz * minxyz * model.kfdelta)
					{

						double t = l*x*bar.p[ind].x  + m*y*bar.p[ind].y +  n*z*bar.p[ind].z;
						double ak = exp(ralph*(x*x*l*l + y*y*m*m + z*z*n*n))/(x*x*l*l + y*y*m*m + z*z*n*n);
						double scos = _scos[l*(kf.y*2+1)*(kf.z*2+1) + (m+kf.y)*(2*kf.z+1) + n+kf.z];
						double ssin = _ssin[l*(kf.y*2+1)*(kf.z*2+1) + (m+kf.y)*(2*kf.z+1) + n+kf.z];
//						if (ind==0) if (m==2 && n ==3) printf("%4d %4d %4d --> %d\n", l,m,n,l*(kf.y*2+1)*(kf.z*2+1) + (m+kf.y)*(2*kf.z+1) + n+kf.z);
//						if (ind==0) if (l==2 && m ==3) printf("%4d << %d\n", n,   n+kf.z);
//						if (ind==0)if (m==2 && n==3) printf("%4d %4d %4d :GPU: %lf %lf\n", l,m,n,scos,ssin);
						double qfac = ak * ( sin(t)*scos - cos(t)*ssin );
//						if (ind ==0) if (l>=8) printf("lmn: %4d, %4d, %4d  >>> scos=%10.4f\tssin=%10.4f\tak=%10.4e\tqfac=%10.4f\t     %e <= %e \n",l,m,n, scos, ssin,ak,qfac,l*x*l*x + m*y*m*y + n*z*n*z , model.kfourier * model.kfourier * minxyz * minxyz * model.kfdelta);
						bar.f[ind].x += l*x*qfac;
						bar.f[ind].y += m*y*qfac;
						bar.f[ind].z += n*z*qfac;
//						if (ind ==0) if (l==0 && m ==1) printf("lmn: %d, %d, %d  <<< scos=%f\tssin=%f\tak=%e\tqfac=%e\tbar.f[0].x=%e\n",l,m,n, scos, ssin,ak,qfac,bar.f[ind].x);
					}
				}
				nmin = -kf.z;
			}
			mmin = -kf.y;
		}
		double qfac = 2.0 * PI / (model.box.x * model.box.y * model.box.z ) * 4.0;
		bar.f[ind].x *= qfac;
		bar.f[ind].y *= qfac;
		bar.f[ind].z *= qfac;

	}

}

void calcFourierForce_CPU(Bar bar,Model model)
{

	memset((void*)bar.f,0,sizeof(bar.f[0])*bar.n);


	double x = 2.0 * PI / model.box.x;
	double y = 2.0 * PI / model.box.y;
	double z = 2.0 * PI / model.box.z;


	double ralph = -0.25 / (model.alpha * model.alpha);

	double minxyz = x < y ? x : y;
	minxyz = z < minxyz ? z : minxyz;

	int nmin =1;
	int mmin =0;

	double kmax = std::min(std::min( x, y),z) * model.kfourier ;


	int3 kf;

	kf.x  = ceil( kmax/x*model.kfdelta+0.5 );
    kf.y  = ceil( kmax/y*model.kfdelta+0.5 );
    kf.z  = ceil( kmax/z*model.kfdelta+0.5 );

	for (int l =0;l<=kf.x;l++){
		for (int m=mmin;m<=kf.y;m++){
			//fprintf("l=%d \t Mmin = %d\tNmin = %d\n",l,mmin,nmin);
			for (int n=nmin;n<=kf.z;n++){
				if (    l*x*l*x +
								m*y*m*y +
								n*z*n*z  <= model.kfourier * model.kfourier * minxyz * minxyz * model.kfdelta)
				{
					double scos = 0;
					double ssin = 0;

					for (int i=0;i<bar.n;i++){
						double t = l*x*bar.p[i].x  + m*y*bar.p[i].y +  n*z*bar.p[i].z;
						scos += cos(t)*model.one[bar.id[i]];
						ssin += sin(t)*model.one[bar.id[i]];
					}




					double ak = exp(ralph*(x*x*l*l + y*y*m*m + z*z*n*n))/(x*x*l*l + y*y*m*m + z*z*n*n);

					double tmp = x*x*l*l + y*y*m*m + z*z*n*n;
					if (l==0 && m ==1) printf("lmn: %d, %d, %d  >>> %f %f %e\n",l,m,n, scos, ssin,ak);

					for (int i=0;i<bar.n;i++){
						double t = l*x*bar.p[i].x  + m*y*bar.p[i].y +  n*z*bar.p[i].z;
						double qfac = ak * ( sin(t)*scos - cos(t)*ssin );
						if (bar.id[i]==0) if (l==0 && m ==1) printf("lmn: %d, %d, %d  >>> %f %f %e %e\n",l,m,n, scos, ssin,ak,qfac);
						bar.f[i].x += l*x*qfac;
						bar.f[i].y += m*y*qfac;
						bar.f[i].z += n*z*qfac;
					}

				}

			}
			nmin = -kf.z;
		}
		mmin = -kf.y;
	}

	double qfac = 2.0 * PI / (model.box.x * model.box.y * model.box.z ) * 4.0;

	for (int i=0;i<bar.n;i++){
		bar.f[i].x *= qfac;
		bar.f[i].y *= qfac;
		bar.f[i].z *= qfac;
	}
}
