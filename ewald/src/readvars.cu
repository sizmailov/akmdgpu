#include "readvars.h"
#include <cstdio>
#include <cstdlib>

#include <cuda.h>

#include "gpuassert.h"

void read_variables(Bar &bar, Model &model, ForceField &ffield) {
	char filename[] = "/home/sergei/.cuda-workspace/ewald/res/var_dump";
	FILE *fin = fopen(filename, "r");

	if (fin == NULL) {
		fprintf(stderr, "Error: \"%s\" does not exist.", filename);
		exit(1);
	}

	fscanf(fin, "%lf", &model.box.x); // 1
	fscanf(fin, "%lf", &model.box.y); //2
	fscanf(fin, "%lf", &model.box.z); //3
	fscanf(fin, "%lf", &bar.potees); //4
	fscanf(fin, "%lf", &bar.poteLJ); //5
	fscanf(fin, "%lf", &bar.poteHb); //6
	fscanf(fin, "%d", &model.ntypes); //7
	fscanf(fin, "%d", &model.mtypes); // 8 

	bar.imolen.p = (double *) malloc(
			model.ntypes * model.ntypes * sizeof(double));
	bar.imolen.n = model.ntypes;

	for (int i = 0; i < model.ntypes; i++)
		for (int j = 0; j < model.ntypes; j++)
			fscanf(fin, "%lf", &arr_val(bar.imolen,j,i)); //9

	fscanf(fin, "%lf", &bar.vireLJ); //10
	fscanf(fin, "%lf", &bar.vireHb); //11
	fscanf(fin, "%d", &bar.n); //12
	fscanf(fin, "%d", &model.matoms); //13

	bar.f = (Point *) malloc(bar.n * sizeof(Point));
	bar.p = (Point *) malloc(bar.n * sizeof(Point));

	for (int i = 0; i < bar.n; i++)
		fscanf(fin, "%lf", &bar.f[i].x); //14
	for (int i = 0; i < bar.n; i++)
		fscanf(fin, "%lf", &bar.f[i].y); //15
	for (int i = 0; i < bar.n; i++)
		fscanf(fin, "%lf", &bar.f[i].z); //16

	fscanf(fin, "%d", &model.nmolec); //17
	fscanf(fin, "%d", &model.mmolec); //18

	model.natoms = bar.n;

	model.indexe = (int *) malloc(model.nmolec * sizeof(int));
	model.intype = (int *) malloc(model.nmolec * sizeof(int));
	model.bigmol = (int *) malloc(model.ntypes * sizeof(int));
	model.moleat = (int *) malloc(bar.n * sizeof(int));

	model.elmode = (int *) malloc(bar.n * sizeof(int));
	model.one = (double *) malloc(bar.n * sizeof(double));
	model.nextat = (int *) malloc(bar.n * sizeof(int));

	for (int i = 0; i < model.nmolec; i++)
		fscanf(fin, "%d,", &model.indexe[i]); //19
	for (int i = 0; i < bar.n; i++)
		fscanf(fin, "%lf,", &bar.p[i].x); //20
	for (int i = 0; i < bar.n; i++)
		fscanf(fin, "%lf,", &bar.p[i].y); //21
	for (int i = 0; i < bar.n; i++)
		fscanf(fin, "%lf,", &bar.p[i].z); //22
	for (int i = 0; i < model.nmolec; i++)
		fscanf(fin, "%d,", &model.intype[i]); //23
	for (int i = 0; i < bar.n; i++)
		fscanf(fin, "%d,", &model.moleat[i]); //24
	for (int i = 0; i < model.ntypes; i++)
		fscanf(fin, "%d,", &model.bigmol[i]); //25
	for (int i = 0; i < bar.n; i++)
		fscanf(fin, "%d,", &model.elmode[i]); //26
	for (int i = 0; i < bar.n; i++)
		fscanf(fin, "%lf,", &model.one[i]); //27
	for (int i = 0; i < bar.n; i++)
		fscanf(fin, "%d,", &model.nextat[i]); //28

	fscanf(fin, "%d", &model.st2bns); //29
	fscanf(fin, "%lf", &model.r2cuto); //30
	fscanf(fin, "%lf", &model.boxi.x); //31
	fscanf(fin, "%lf", &model.boxi.y); //32
	fscanf(fin, "%lf", &model.boxi.z); //33
	fscanf(fin, "%d", &ffield.mmodes); //34

	ffield.sixi.p = (double *) malloc(
			ffield.mmodes * ffield.mmodes * sizeof(double));
	ffield.sixi.n = ffield.mmodes;
	ffield.twli.p = (double *) malloc(
			ffield.mmodes * ffield.mmodes * sizeof(double));
	ffield.twli.n = ffield.mmodes;
	ffield.six.p = (double *) malloc(
			ffield.mmodes * ffield.mmodes * sizeof(double));
	ffield.six.n = ffield.mmodes;
	ffield.twl.p = (double *) malloc(
			ffield.mmodes * ffield.mmodes * sizeof(double));
	ffield.twl.n = ffield.mmodes;
	ffield.ljdeli.p = (double *) malloc(
			ffield.mmodes * ffield.mmodes * sizeof(double));
	ffield.ljdeli.n = ffield.mmodes;
	ffield.ljdel.p = (double *) malloc(
			ffield.mmodes * ffield.mmodes * sizeof(double));
	ffield.ljdel.n = ffield.mmodes;
	ffield.hba.p = (double *) malloc(
			ffield.mmodes * ffield.mmodes * sizeof(double));
	ffield.hba.n = ffield.mmodes;
	ffield.hbb.p = (double *) malloc(
			ffield.mmodes * ffield.mmodes * sizeof(double));
	ffield.hbb.n = ffield.mmodes;
	ffield.hbonded.p = (int *) malloc(
			ffield.mmodes * ffield.mmodes * sizeof(int));
	ffield.hbonded.n = ffield.mmodes;

	for (int i = 0; i < ffield.mmodes; i++)
		for (int j = 0; j < ffield.mmodes; j++)
			fscanf(fin, "%lf", &arr_val(ffield.sixi,j,i)); //35
	for (int i = 0; i < ffield.mmodes; i++)
		for (int j = 0; j < ffield.mmodes; j++)
			fscanf(fin, "%lf", &arr_val(ffield.twli,j,i)); //36
	for (int i = 0; i < ffield.mmodes; i++)
		for (int j = 0; j < ffield.mmodes; j++)
			fscanf(fin, "%lf", &arr_val(ffield.ljdeli,j,i)); //37
	for (int i = 0; i < ffield.mmodes; i++)
		for (int j = 0; j < ffield.mmodes; j++)
			fscanf(fin, "%lf", &arr_val(ffield.six,j,i)); //38
	for (int i = 0; i < ffield.mmodes; i++)
		for (int j = 0; j < ffield.mmodes; j++)
			fscanf(fin, "%lf", &arr_val(ffield.twl,j,i)); //39
	for (int i = 0; i < ffield.mmodes; i++)
		for (int j = 0; j < ffield.mmodes; j++)
			fscanf(fin, "%lf", &arr_val(ffield.ljdel,j,i)); //40
	for (int i = 0; i < ffield.mmodes; i++)
		for (int j = 0; j < ffield.mmodes; j++)
			fscanf(fin, "%d", &arr_val(ffield.hbonded,j,i)); //41
	for (int i = 0; i < ffield.mmodes; i++)
		for (int j = 0; j < ffield.mmodes; j++)
			fscanf(fin, "%lf", &arr_val(ffield.hba,j,i)); //42
	for (int i = 0; i < ffield.mmodes; i++)
		for (int j = 0; j < ffield.mmodes; j++)
			fscanf(fin, "%lf", &arr_val(ffield.hbb,j,i)); //43

	fscanf(fin, "%lf", &model.alpha); //44

	fclose(fin);

	bar.id = (int *) malloc(bar.n * sizeof(int));
	for (int i = 0; i < bar.n; i++)
		bar.id[i] = i;

}

void allocBarOnDevice(const Bar &bar, Bar &dev_bar) {
//
//   allocated N*scale elements in f and N in 
//
	gpuErrchk(cudaMalloc((void** ) &dev_bar.p, sizeof(bar.p[0]) * bar.n));
	gpuErrchk(cudaMalloc((void** ) &dev_bar.f, sizeof(bar.f[0]) * bar.n));
	gpuErrchk(cudaMalloc((void** ) &dev_bar.id, sizeof(bar.id[0]) * bar.n));
	dev_bar.n = bar.n;

	unsigned long rows = bar.imolen.n;
	double *imolen_;
	gpuErrchk(
			cudaMalloc( (void**) &imolen_,sizeof(arr_val(bar.imolen,0,0))*rows*rows));
	dev_bar.imolen.p = imolen_;
	dev_bar.imolen.n = rows;
}

void copyBarToDevice(const Bar &bar, Bar &dev_bar) {

	gpuErrchk(
			cudaMemcpy(dev_bar.p, bar.p, sizeof(bar.p[0]) * bar.n,
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_bar.f, bar.f, sizeof(bar.f[0]) * bar.n,
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_bar.id, bar.id, sizeof(bar.id[0]) * bar.n,
					cudaMemcpyHostToDevice));

	unsigned int rows = bar.imolen.n;
	gpuErrchk(
			cudaMemcpy(dev_bar.imolen.p, bar.imolen.p,
					sizeof(double) * rows * rows, cudaMemcpyHostToDevice));

	dev_bar.potees = bar.potees;
	dev_bar.poteLJ = bar.poteLJ;
	dev_bar.poteHb = bar.poteHb;
	dev_bar.vireLJ = bar.vireLJ;
	dev_bar.vireHb = bar.vireHb;

}

void freeBarOnDevice(Bar &dev_bar) {
	gpuErrchk(cudaFree(dev_bar.p));
	gpuErrchk(cudaFree(dev_bar.f));
	gpuErrchk(cudaFree(dev_bar.id));
	gpuErrchk(cudaFree(dev_bar.imolen.p));
	dev_bar.n = 0;
	dev_bar.imolen.n = 0;
}

void allocForceFieldOnDevice(const ForceField ff, ForceField &dev_ff) {
	gpuErrchk(
			cudaMalloc((void** ) &dev_ff.six,
					ff.mmodes * ff.mmodes * sizeof(double)));
	gpuErrchk(
			cudaMalloc((void** ) &dev_ff.sixi,
					ff.mmodes * ff.mmodes * sizeof(double)));
	gpuErrchk(
			cudaMalloc((void** ) &dev_ff.twl,
					ff.mmodes * ff.mmodes * sizeof(double)));
	gpuErrchk(
			cudaMalloc((void** ) &dev_ff.twli,
					ff.mmodes * ff.mmodes * sizeof(double)));
	gpuErrchk(
			cudaMalloc((void** ) &dev_ff.ljdel,
					ff.mmodes * ff.mmodes * sizeof(double)));
	gpuErrchk(
			cudaMalloc((void** ) &dev_ff.ljdeli,
					ff.mmodes * ff.mmodes * sizeof(double)));
	gpuErrchk(
			cudaMalloc((void** ) &dev_ff.hba,
					ff.mmodes * ff.mmodes * sizeof(double)));
	gpuErrchk(
			cudaMalloc((void** ) &dev_ff.hbb,
					ff.mmodes * ff.mmodes * sizeof(double)));
	gpuErrchk(
			cudaMalloc((void** ) &dev_ff.hbonded,
					ff.mmodes * ff.mmodes * sizeof(int)));

	dev_ff.six.n = ff.mmodes;
	dev_ff.sixi.n = ff.mmodes;
	dev_ff.twl.n = ff.mmodes;
	dev_ff.twli.n = ff.mmodes;
	dev_ff.ljdel.n = ff.mmodes;
	dev_ff.ljdeli.n = ff.mmodes;
	dev_ff.hba.n = ff.mmodes;
	dev_ff.hbb.n = ff.mmodes;
	dev_ff.hbonded.n = ff.mmodes;
}
void copyForceFieldToDevice(const ForceField ff, ForceField &dev_ff) {
//  Requirements (presumptions):
//  1) all arrays in dev_ff are alloced at this point 
//  2) mmodes same in ff and dev_ff
// 
	unsigned long mm2 = ff.mmodes * ff.mmodes;
	gpuErrchk(
			cudaMemcpy(dev_ff.six.p, ff.six.p, mm2 * sizeof(double),
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_ff.sixi.p, ff.sixi.p, mm2 * sizeof(double),
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_ff.twl.p, ff.twl.p, mm2 * sizeof(double),
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_ff.twli.p, ff.twli.p, mm2 * sizeof(double),
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_ff.ljdel.p, ff.ljdel.p, mm2 * sizeof(double),
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_ff.ljdeli.p, ff.ljdeli.p, mm2 * sizeof(double),
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_ff.hba.p, ff.hba.p, mm2 * sizeof(double),
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_ff.hbb.p, ff.hbb.p, mm2 * sizeof(double),
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_ff.hbonded.p, ff.hbonded.p, mm2 * sizeof(int),
					cudaMemcpyHostToDevice));
}
void freeForceFieldOnDevice(ForceField &dev_ff) {
	gpuErrchk(cudaFree(dev_ff.six.p));
	gpuErrchk(cudaFree(dev_ff.sixi.p));
	gpuErrchk(cudaFree(dev_ff.twl.p));
	gpuErrchk(cudaFree(dev_ff.twli.p));
	gpuErrchk(cudaFree(dev_ff.ljdel.p));
	gpuErrchk(cudaFree(dev_ff.ljdeli.p));
	gpuErrchk(cudaFree(dev_ff.hba.p));
	gpuErrchk(cudaFree(dev_ff.hbb.p));
	gpuErrchk(cudaFree(dev_ff.hbonded.p));
	dev_ff.six.n = 0;
	dev_ff.sixi.n = 0;
	dev_ff.twl.n = 0;
	dev_ff.twli.n = 0;
	dev_ff.ljdel.n = 0;
	dev_ff.ljdeli.n = 0;
	dev_ff.hba.n = 0;
	dev_ff.hbb.n = 0;
	dev_ff.hbonded.n = 0;
}

void allocModelOnDevice(const Model &m, Model &dev_m) {
	dev_m.matoms = m.matoms;
	dev_m.natoms = m.natoms;
	dev_m.ntypes = m.ntypes;
	dev_m.mtypes = m.mtypes;
	dev_m.nmolec = m.nmolec;
	dev_m.mmolec = m.mmolec;
	dev_m.st2bns = m.st2bns;

	gpuErrchk(cudaMalloc((void** ) &dev_m.indexe, sizeof(int) * m.nmolec));
	gpuErrchk(cudaMalloc((void** ) &dev_m.moleat, sizeof(int) * m.natoms));
	gpuErrchk(cudaMalloc((void** ) &dev_m.bigmol, sizeof(int) * m.ntypes));
	gpuErrchk(cudaMalloc((void** ) &dev_m.elmode, sizeof(int) * m.natoms));
	gpuErrchk(cudaMalloc((void** ) &dev_m.nextat, sizeof(int) * m.natoms));
	gpuErrchk(cudaMalloc((void** ) &dev_m.intype, sizeof(int) * m.nmolec));

	gpuErrchk(cudaMalloc((void** ) &dev_m.one, sizeof(double) * m.natoms));

}

void copyModelToDevice(const Model &m, Model &dev_m) {
	gpuErrchk(
			cudaMemcpy(dev_m.indexe, m.indexe, sizeof(int) * m.nmolec,
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_m.moleat, m.moleat, sizeof(int) * m.natoms,
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_m.bigmol, m.bigmol, sizeof(int) * m.ntypes,
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_m.elmode, m.elmode, sizeof(int) * m.natoms,
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_m.nextat, m.nextat, sizeof(int) * m.natoms,
					cudaMemcpyHostToDevice));
	gpuErrchk(
			cudaMemcpy(dev_m.intype, m.intype, sizeof(int) * m.nmolec,
					cudaMemcpyHostToDevice));
	fprintf(stderr,"Copy %d atom charges to Device\n",m.natoms);
	gpuErrchk(
			cudaMemcpy(dev_m.one, m.one, sizeof(double) * m.natoms,
					cudaMemcpyHostToDevice));

	dev_m.box = m.box;
	dev_m.boxi = m.boxi;
	dev_m.alpha = m.alpha;
	dev_m.r2cuto = m.r2cuto;
	dev_m.kfdelta = m.kfdelta;
	dev_m.kfourier = m.kfourier;
}
void freeModelOnDevice(Model &dev_m) {
	dev_m.natoms = 0;
	dev_m.ntypes = 0;
	dev_m.nmolec = 0;
	dev_m.st2bns = 0;

	gpuErrchk(cudaFree(dev_m.indexe));
	gpuErrchk(cudaFree(dev_m.moleat));
	gpuErrchk(cudaFree(dev_m.bigmol));
	gpuErrchk(cudaFree(dev_m.elmode));
	gpuErrchk(cudaFree(dev_m.nextat));
	gpuErrchk(cudaFree(dev_m.intype));

	gpuErrchk(cudaFree(dev_m.one));
}

