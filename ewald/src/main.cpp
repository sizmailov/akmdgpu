#include <cstdio>
#include <cmath>

#include <ctime>

#include "cuda_routines.h"
#include "grid.h"

void print_vector(Point v[], int n) {
	for (int i = 0; i < n; i++) {
		printf("%.5e %.5e %.5e\n", v[i].x, v[i].y, v[i].z);
	}
}

int main(int argc, char * argv[]) {
	printf("Hello world");

	Bar bar;
	Model model;
	ForceField ff;

	read_variables(bar, model, ff);

	model.kfdelta = 1.0+1e-10;
	model.kfourier = 15;


	Grid grid;
	grid.orderToBars(bar.p, bar.id, bar.n, model.box, sqrt(model.r2cuto));

	int_coord_3 begin = { 0, 0, 0 };
	int_coord_3 end = { grid.getChunksNumberX(), grid.getChunksNumberY(),
			grid.getChunksNumberZ() };

	printf("Grid (%3ld,%3ld,%3ld)\n", grid.getChunksNumberX(),
			grid.getChunksNumberY(), grid.getChunksNumberZ());




	init_cuda(bar, model, ff);

	clock_t begin_ = clock();

	const int GPU = 1;
	if (GPU==1){
		calcFoureir(bar, model);
	}else
	{
		calcFourierForce_CPU(bar,model);
	}
//	calculate_force_cuda(grid,bar,begin,end);

	clock_t end_ = clock();
	double elapsed_secs = double(end_ - begin_) / CLOCKS_PER_SEC;

	finalize_cuda();

	printf("Rcutoff = %lf\n", sqrt(model.r2cuto));
//	printf("Grid (%3ld,%3ld,%3ld)\n", grid.getChunksNumberX(),
//			grid.getChunksNumberY(), grid.getChunksNumberZ());
	printf("cuda elapsed time : %lf\n", elapsed_secs);
//
//	printf("PoteES = %lf\n", bar.potees);
//	printf("PoteLJ = %lf\n", bar.poteLJ);
//	printf("PoteHb = %lf\n", bar.poteHb);
//	printf("VireHb = %lf\n", bar.vireHb);
//	printf("VireLJ = %lf\n", bar.vireLJ);

//   print_vector(bar.f,bar.n);
//
//	for (int i = 0; i < bar.n; i++) {
//     printf("%5d %le %le %le \n",bar.id[i],bar.f[i].x,bar.f[i].y,bar.f[i].z);
//	}

	return 0;
}
